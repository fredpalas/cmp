<?php


namespace App\Infrastructure\Repository\Doctrine\Types;

use App\Domain\Model\VideoId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use InvalidArgumentException;
use Ramsey\Uuid\Doctrine\UuidType;

class VideoIdType extends UuidType
{
    const NAME = 'video_id';

    /**
     * @param \Ramsey\Uuid\UuidInterface|string|null $value
     * @param AbstractPlatform $platform
     * @return \App\Domain\Model\Common\AbstractId|mixed|\Ramsey\Uuid\UuidInterface|string|null
     * @throws ConversionException
     * @throws \App\Domain\Model\Common\Exception\InvalidIdentityException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (empty($value)) {
            return null;
        }

        if ($value instanceof VideoId) {
            return $value;
        }

        try {
            $uuid = VideoId::fromString($value);
        } catch (InvalidArgumentException $exception) {
            throw ConversionException::conversionFailed($value, static::NAME);
        }

        return $uuid;
    }

    /**
     * @param \Ramsey\Uuid\UuidInterface|string|null $value
     * @param AbstractPlatform $platform
     * @return mixed|string|null
     * @throws ConversionException
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (empty($value)) {
            return null;
        }

        if ($value instanceof VideoId) {
            return (string) $value;
        }

        throw ConversionException::conversionFailed($value, static::NAME);
    }
}