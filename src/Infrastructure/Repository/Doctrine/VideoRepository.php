<?php


namespace App\Infrastructure\Repository\Doctrine;


use App\Domain\Model\Common\Exception\VideoNotFoundException;
use App\Domain\Model\Video;
use App\Domain\Model\VideoId;
use App\Domain\Repository\VideoRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class VideoRepository implements VideoRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /** @var EntityRepository */
    private $videos;

    /**
     * VideoRepository constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->videos  = $this->manager->getRepository(Video::class);
    }

    /**
     * @param VideoId $videoId
     * @return Video
     * @throws VideoNotFoundException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function get(VideoId $videoId): Video
    {
        $wish = $this->videos
            ->createQueryBuilder('video')
            ->select(['wish'])
            ->andWhere('wish.id = :id')
            ->setParameter('id', $videoId)
            ->getQuery()
            ->getSingleResult();

        if (null === $wish) {
            throw new VideoNotFoundException($videoId);
        }

        return $wish;
    }

    /**
     * @param Video $video
     */
    public function put(Video $video)
    {
        $video = $this->manager->merge($video);
        $this->manager->persist($video);
        $this->manager->flush();
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function slice(int $offset, int $limit): array
    {
        $query = $this->videos
            ->createQueryBuilder('video')
            ->select(['video'])
            ->orderBy('video.createdAt', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery();

        return (new Paginator($query, true))
            ->getIterator()
            ->getArrayCopy();
    }

    /**
     * @return VideoId
     * @throws \Exception
     */
    public function getNextVideoId(): VideoId
    {
        return VideoId::next();
    }
}