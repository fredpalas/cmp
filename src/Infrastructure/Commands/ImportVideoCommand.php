<?php


namespace App\Infrastructure\Commands;


use App\Application\ImportVideoInterface;
use App\Application\ParseFileInterface;
use App\Domain\Model\VideoToImport;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ImportVideoCommand extends Command
{

    protected static $defaultName = 'app:import-video';
    /** @var OutputInterface */
    private $output;
    /** @var  InputInterface */
    private $input;

    const PROVIDER_LIST = [
        'flub' => [
            'file' => 'flub.yaml',
            'type' => 'yaml',
        ],
        'glorf' => [
            'file' => 'glorf.json',
            'type' => 'json',
        ],
    ];

    const PROVIDER_FOLDER = 'provider';

    private $parseFile;
    private $kernel;
    private $validator;
    private $importVideo;

    public function __construct(
        ParseFileInterface $parseFile,
        KernelInterface $kernel,
        ValidatorInterface $validator,
        ImportVideoInterface $importVideo,
        string $name = null
    ) {
        $this->parseFile = $parseFile;
        $this->kernel = $kernel;
        $this->validator = $validator;
        $this->importVideo = $importVideo;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->addArgument('provider', InputOption::VALUE_REQUIRED, 'File name of teh provider on "{project_folder}/provider"');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->input = $input;
        $provider = $input->getArgument('provider');
        $providers = self::PROVIDER_LIST;
        if (!array_key_exists($provider, $providers)) {
            throw new \Exception('Provider not Found');
        }

        $providerConf = self::PROVIDER_LIST[$provider];
        $file = new File($this->getFilePath($providerConf['file']));

        switch ($provider) {
            case 'flub':
                $this->processFlub($file);
                break;
            case 'glorf':
                $this->processGlorf($file);
                break;
        }
    }

    private function getFilePath(string $file)
    {
        return $this->kernel->getProjectDir().'/'.self::PROVIDER_FOLDER.'/'.$file;
    }

    private function processFlub(File $file)
    {
        $array = $this->parseFile->parseYml($file);

        $constraint = new Assert\Collection(
            [
                'name' => new Assert\Required(),
                'url' => [
                    new Assert\Required(),
                    new Assert\Url(),
                ],
                'labels' => new Assert\Optional(
                    [
                        new Assert\Type('string'),
                    ]
                ),
            ]
        );

        foreach ($array as $key => $item) {

            if (!$this->validateConstraintItem($item, $constraint, $key)) {
                continue;
            }

            $this->output->writeln(
                [
                    'Importing Video: '.$item['name'].
                    ' Url: '.$item['url'].
                    (isset($item['labels']) ? ' Tags: '.$item['labels'] : ''),
                    '============',
                ]
            );

            $tags = isset($item['labels']) ? explode(',', $item['labels']) : null;

            $videoToImport = new VideoToImport(
                $item['url'],
                $item['name'],
                'flub',
                $tags
            );
            $this->importVideo->importVideo($videoToImport);
        }
    }

    private function processGlorf(File $file)
    {
        $array = $this->parseFile->parseJson($file);
        $constraint = new Assert\Collection(
            [
                'title' => new Assert\Required(),
                'url' => [
                    new Assert\Required(),
                    new Assert\Url(),
                ],
                'tags' => new Assert\Optional(
                    [
                        new Assert\Type('array'),
                        new Assert\Count(['min' => 1]),
                    ]
                ),
            ]
        );

        foreach ($array['videos'] as $key => $item) {

            if (!$this->validateConstraintItem($item, $constraint, $key)) {
                continue;
            }

            $this->output->writeln(
                [
                    'Importing Video: '.$item['title'].
                    ' Url: '.$item['url'].
                    (isset($item['tags']) ? ' Tags: '.implode(',', $item['tags']) : ''),
                    '============',
                ]
            );

            $videoToImport = new VideoToImport(
                $item['url'],
                $item['title'],
                'glorf',
                $item['tags'] ?? null
            );

            $this->importVideo->importVideo($videoToImport);
        }
    }

    private function processErrorBagMessage(ConstraintViolationListInterface $errors)
    {
        if (count($errors) == 0) {
            return;
        }

        foreach ($errors as $error) {
            $this->output->writeln(
                [
                    $error->getInvalidValue().': '.$error->getMessage(),
                ]
            );
        }
    }

    private function validateConstraintItem($item, $constraint, $key)
    {
        $validations = $this->validator->validate($item, $constraint);
        if ($validations->count() > 0) {
            $this->output->writeln(
                [
                    'Errors on value '.$key,
                    '============',
                ]
            );
            $this->processErrorBagMessage($validations);

            return false;
        }

        return true;
    }
}