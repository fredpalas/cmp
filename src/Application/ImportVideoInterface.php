<?php


namespace App\Application;


use App\Domain\Model\VideoToImport;

interface ImportVideoInterface
{
    public function importVideo(VideoToImport $videoToImport): bool;
}