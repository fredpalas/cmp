<?php


namespace App\Application;


use Doctrine\Migrations\Configuration\Exception\JsonNotValid;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser as YamlParser;
use Symfony\Component\Yaml\Yaml;

class ParseFiles implements ParseFileInterface
{

    private $yamlFileLoader;

    public function __construct(YamlParser $yamlFileLoader)
    {
        $this->yamlFileLoader = $yamlFileLoader;
    }

    /**
     * @param File $file
     * @return array
     */
    public function parseYml(File $file): array
    {
        try {
            $yaml = $this->yamlFileLoader->parseFile($file->getRealPath(), Yaml::PARSE_CONSTANT);
        } catch (ParseException $e) {
            throw new \InvalidArgumentException(sprintf('The file "%s" does not contain valid YAML.', $file->getRealPath()), 0, $e);
        }

        return $yaml;
    }

    /**
     * @param File $file
     * @return array
     */
    public function parseJson(File $file): array
    {
        $contents = $file->openFile()->fread($file->getSize());

        $json = json_decode($contents, true);

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                break;
            case JSON_ERROR_CTRL_CHAR:
                throw new JsonNotValid('JSON_ERROR_CTRL_CHAR');
                break;
            case JSON_ERROR_SYNTAX:
                throw new JsonNotValid('JSON_ERROR_SYNTAX');
                break;
            case JSON_ERROR_UTF8:
                throw new JsonNotValid('JSON_ERROR_UTF8');
                break;
            default:
                throw new JsonNotValid();
                break;
        }

        return $json;
    }
}