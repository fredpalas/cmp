<?php


namespace App\Application;


class HttpClient implements HttpClientInterface
{

    private const TEST_URL = 'https://teamcmp.com/img/logo@2x.png';
    /** @var bool */
    private $debug;

    public function __construct(bool $debug = false)
    {
        $this->debug = $debug;
    }

    /**
     * @param string $url
     * @param array $options
     * @return bool|resource
     */
    public function get(string $url, array $options = [])
    {
        if ($this->debug) {
            $url = self::TEST_URL;
        }

        $content = fopen($url, 'r');

        return $content;
    }

}