<?php


namespace App\Application;


interface DownloaderInterface
{
    public function downloadFromUrl(string $url, string $pathLocation): string;
}