<?php


namespace App\Application;


class FileSystem implements FileSystemInterface
{

    /**
     * @param string $path
     * @param resource $resource
     */
    public function move(string $path, $resource)
    {
        file_put_contents($path, $resource);
    }
}