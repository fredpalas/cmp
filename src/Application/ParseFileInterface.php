<?php


namespace App\Application;


use Symfony\Component\HttpFoundation\File\File;

interface ParseFileInterface
{
    public function parseYml(File $file): array;

    public function parseJson(File $file): array;
}