<?php


namespace App\Application;


interface HttpClientInterface
{
    /**
     * @param string $url
     * @param array $options
     * @return resource
     */
    public function get(string $url, array $options = []);
}