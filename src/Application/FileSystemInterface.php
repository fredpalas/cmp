<?php


namespace App\Application;


interface FileSystemInterface
{
    public function move(string $path, $resource);
}