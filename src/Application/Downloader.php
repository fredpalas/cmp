<?php


namespace App\Application;


use Symfony\Component\Filesystem\Filesystem as FilesystemAlias;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Mime\MimeTypes;

class Downloader implements DownloaderInterface
{

    private $httpClient;
    private $fileSystem;
    private $filesystemSymfony;

    public function __construct(HttpClientInterface $httpClient, FileSystemInterface $fileSystem, FilesystemAlias $filesystem)
    {
        $this->httpClient = $httpClient;
        $this->fileSystem = $fileSystem;
        $this->filesystemSymfony = $filesystem;
    }

    public function downloadFromUrl(string $url, string $pathLocation): string
    {
        $file = $this->httpClient->get($url);
        $meta = stream_get_meta_data($file);
        $mimetype = null;
        foreach ($meta['wrapper_data'] as $header) {
            $matches = null;
            if (preg_match('/^content-type: ([^\s]+)/i', $header, $matches)) {
                $mimetype = $matches[1];
                break;
            }
        }

        $extension = $this->guessExtension($mimetype);

        $filename = md5($meta["uri"]).'-'.date('Y-m-d-H-i-s').'.'.$extension;
        $filenamePath = $pathLocation.'/'.$filename;

        if (!$this->filesystemSymfony->exists($pathLocation)) {
            $this->filesystemSymfony->mkdir($pathLocation);
        }

        $this->fileSystem->move($filenamePath, $file);


        return $filenamePath;
    }

    private function guessExtension($mime)
    {
        return MimeTypes::getDefault()->getExtensions($mime)[0] ?? null;
    }
}