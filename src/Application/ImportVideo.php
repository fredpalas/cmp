<?php


namespace App\Application;


use App\Domain\Model\VideoToImport;
use Symfony\Component\HttpKernel\KernelInterface;

class ImportVideo implements ImportVideoInterface
{

    const PATH_LOCATION = 'videos';

    private $downloader;
    private $video;
    private $kernel;

    public function __construct(DownloaderInterface $downloader, VideoInterface $video, KernelInterface $kernel)
    {
        $this->downloader = $downloader;
        $this->video = $video;
        $this->kernel = $kernel;
    }

    public function importVideo(VideoToImport $videoToImport): bool
    {
        $path = $this->downloader->downloadFromUrl($videoToImport->getUrl(), $this->getPath().'/'.$videoToImport->getProvider());

        $this->video->addNewVideo(
            $videoToImport->getUrl(),
            $videoToImport->getTitle(),
            $path,
            true,
            $videoToImport->getTags()
        );

        return true;
    }

    private function getPath(): string
    {
        return $this->kernel->getProjectDir().'/'.self::PATH_LOCATION;
    }
}