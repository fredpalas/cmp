<?php


namespace App\Application;


use App\Domain\Model\VideoId;
use App\Domain\Repository\VideoRepositoryInterface;

class Video implements VideoInterface
{

    private $videos;

    public function __construct(VideoRepositoryInterface $videoRepository)
    {
        $this->videos = $videoRepository;
    }

    /**
     * @param string $url
     * @param string $name
     * @param string $path
     * @param bool $imported
     * @param array $tags
     * @return string
     * @throws \Exception
     */
    public function addNewVideo(string $url, string $name, string $path, bool $imported = false, ?array $tags = []): string
    {
        $videoId = VideoId::next();

        $video = $this->createVideoFromId($videoId, $url, $name, $path, $tags);

        if ($imported) {
            $video->setIsImported();
        }

        $this->videos->put($video);

        return $videoId->getId();
    }

    private function createVideoFromId(
        VideoId $videoId,
        string $url,
        string $name,
        string $path,
        ?array $tags = []
    ): \App\Domain\Model\Video
    {
        $video = new \App\Domain\Model\Video(
            $videoId,
            $url,
            $name,
            $path,
            $tags
        );


        return $video;
    }


    public function setVideoId(VideoId $videoId)
    {

    }
}