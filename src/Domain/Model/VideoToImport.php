<?php


namespace App\Domain\Model;


class VideoToImport
{
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $provider;
    /**
     * @var array | null
     */
    private $tags = [];

    public function __construct(string $url, string $title, string $provider, array $tags = null)
    {
        $this->url = $url;
        $this->title = $title;
        $this->tags = $tags;
        $this->provider = $provider;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getTags(): ?array
    {
        return $this->tags;
    }

    /**
     * @return string
     */
    public function getProvider(): string
    {
        return $this->provider;
    }
}