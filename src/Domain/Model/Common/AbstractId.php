<?php


namespace App\Domain\Model\Common;

use App\Domain\Model\Common\Exception\InvalidIdentityException;
use Ramsey\Uuid\Exception\InvalidUuidStringException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class AbstractId
{
    /**
     * @var UuidInterface
     */
    protected $id;

    /**
     * AbstractId constructor.
     * @param UuidInterface $id
     */
    private function __construct(UuidInterface $id)
    {
        $this->id = $id;
    }

    /**
     * @param string $id
     * @return AbstractId
     * @throws InvalidIdentityException
     */
    public static function fromString(string $id)
    {
        try {
            return new static(Uuid::fromString($id));
        } catch (InvalidUuidStringException $exception) {
            throw new InvalidIdentityException($id);
        }
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public static function next()
    {
        return new static(Uuid::uuid4());
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @param AbstractId $id
     * @return bool
     */
    public function equalTo(AbstractId $id): bool
    {
        return $this->getId() === $id->getId() &&
            get_class($this) === get_class($id);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getId();
    }
}