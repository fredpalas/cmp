<?php


namespace App\Domain\Model\Common\Exception;


use Exception;

class VideoNotFoundException extends Exception implements DomainExceptionInterface, NotFoundExceptionInterface
{
    public function __construct($videoId)
    {
        parent::__construct('Video not found. ID: ' . (string) $videoId);
    }
}