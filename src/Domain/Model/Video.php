<?php


namespace App\Domain\Model;


use DateTimeImmutable;

class Video
{

    private $id;
    private $url;
    private $title;
    private $tags = [];
    private $path;
    private $imported = false;
    private $createdAt;
    private $updatedAt;

    public function __construct(VideoId $id, string $url, string $title, $path, ?array $tags = null, $createdAt = null)
    {
        $this->id = $id;
        $this->url = $url;
        $this->title = $title;
        $this->tags = $tags;
        $this->path = $path;
        $this->createdAt = $createdAt ?? new DateTimeImmutable();
        $this->updatedAt = $createdAt ?? new DateTimeImmutable();
    }

    /**
     * @return VideoId
     */
    public function getId(): VideoId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setIsImported()
    {
        $this->imported = true;
    }

    /**
     * @return bool
     */
    public function isImported(): bool
    {
        return $this->imported;
    }

}