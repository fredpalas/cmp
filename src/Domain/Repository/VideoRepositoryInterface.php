<?php


namespace App\Domain\Repository;


use App\Domain\Model\Video;
use App\Domain\Model\VideoId;

interface VideoRepositoryInterface
{
    public function get(VideoId $videoId): Video;
    public function put(Video $video);
    public function slice(int $offset, int $limit): array;
    public function getNextVideoId(): VideoId;
}