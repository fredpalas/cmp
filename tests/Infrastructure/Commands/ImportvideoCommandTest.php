<?php


namespace App\Tests\Infrastructure\Commands;

use App\Application\ImportVideo;
use App\Application\ImportVideoInterface;
use App\Application\ParseFileInterface;
use App\Domain\Model\VideoToImport;
use App\Infrastructure\Commands\ImportVideoCommand;
use App\Kernel;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ImportVideoCommandTest extends KernelTestCase
{
    /** @var ParseFileInterface | MockObject */
    private $parseFile;
    /** @var Kernel | MockObject */
    private $kernelService;
    /** @var ValidatorInterface | MockObject */
    private $validator;
    /** @var ImportVideo | MockObject */
    private $importVideo;
    /** @var Application */
    private $application;


    public function setUp()
    {
        $kernel = static::createKernel();
        $this->application = new Application($kernel);
        $this->parseFile = $this->getMockBuilder(ParseFileInterface::class)->disableOriginalConstructor()->getMock();
        self::bootKernel();
        $this->kernelService = self::$kernel;
        $this->validator = $this->getMockBuilder(ValidatorInterface::class)->disableOriginalConstructor()->getMock();
        $this->importVideo = $this->getMockBuilder(ImportVideoInterface::class)->disableOriginalConstructor()->getMock();
        $this->application->add(
            new ImportVideoCommand(
                $this->parseFile,
                $this->kernelService,
                $this->validator,
                $this->importVideo,
                null
            )
        );

    }


    public function testExecuteNotErrors()
    {
        $command = $this->application->find('app:import-video');
        $commandTester = new CommandTester($command);
        $dataGlorf = $this->dataTestGlorf();
        $this->parseFile->method('parseJson')->willReturn($dataGlorf);
        $validatorMock = $this->getMockBuilder(ConstraintViolationList::class)->disableOriginalConstructor()->getMock();
        $this->validator->method('validate')->willReturn($validatorMock);
        $validatorMock->method('count')->willReturn(0);
        $item = $dataGlorf['videos'][0];
        $videoToImport = new VideoToImport(
            $item['url'],
            $item['title'],
            'glorf',
            $item['tags'] ?? null
        );

        $this->importVideo->method('importVideo')->with($videoToImport);
        $commandTester->execute(
            [
                'command' => $command->getName(),
                'provider' => 'glorf',
            ]
        );

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertContains(
            'Importing Video: science experiment goes wrong Url: http://glorf.com/videos/3 Tags: microwave,cats,peanutbutter',
            $output
        );

    }

    private function dataTestGlorf()
    {
        return [
            'videos' => [
                [
                    "tags" => [
                        "microwave",
                        "cats",
                        "peanutbutter",
                    ],
                    "url" => "http://glorf.com/videos/3",
                    "title" => "science experiment goes wrong",
                ],
            ],
        ];
    }
}