<?php


namespace App\Tests\Application;

use App\Application\DownloaderInterface;
use App\Application\ImportVideo;
use App\Application\VideoInterface;
use App\Domain\Model\VideoToImport;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use App\Kernel;


class ImportVideoTest extends TestCase
{
    /** @var  DownloaderInterface | MockObject */
    private $downloader;
    /** @var VideoInterface | MockObject */
    private $video;
    /** @var Kernel | MockObject */
    private $kernel;
    /** @var ImportVideo */
    private $importVideo;

    const URL = 'http://anyurl.any/file.jpg';
    const TITLE = 'AnyTitle';
    const PATH = '/Any/Path';
    const PROVIDER = 'AnyProvider';
    const FILE_NAME = 'any_file_name.jpg';
    const Tags = ['any', 'tags'];

    public function setUp()
    {
        $this->downloader = $this->getMockBuilder(DownloaderInterface::class)->disableOriginalConstructor()->getMock();
        $this->video = $this->getMockBuilder(VideoInterface::class)->disableOriginalConstructor()->getMock();
        $this->kernel = $this->getMockBuilder(Kernel::class)->disableOriginalConstructor()->getMock();
        $this->importVideo = new ImportVideo($this->downloader, $this->video, $this->kernel);
    }

    public function testImportVideo()
    {
        $videoToImport = $this->getVideoToImport();

        $this->kernel->method('getProjectDir')->willReturn(self::PATH);

        $path = self::PATH.'/'.ImportVideo::PATH_LOCATION.'/'.$videoToImport->getProvider();
        $fullPath = $path.'/'.self::FILE_NAME;
        $this->downloader->expects(self::once())
            ->method('downloadFromUrl')
            ->with($videoToImport->getUrl(), $path)
            ->willReturn($fullPath);

        $this->video->method('addNewVideo')->with(
            $videoToImport->getUrl(),
            $videoToImport->getTitle(),
            $fullPath,
            true,
            $videoToImport->getTags()
        );

        $response = $this->importVideo->importVideo($videoToImport);

        $this->assertIsBool($response);
    }


    private function getVideoToImport()
    {
        return new VideoToImport(
            self::URL,
            self::TITLE,
            self::PROVIDER,
            self::Tags
        );
    }
}