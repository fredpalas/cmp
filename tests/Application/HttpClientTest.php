<?php


namespace App\Tests\Application;


use App\Application\HttpClient;
use PHPUnit\Framework\TestCase;

/**
 * HttpClient is so simple Test can be better with help of https://github.com/bovigo/vfs-stream-examples
 * Class HttpClientTest
 * @package App\Tests\Application
 */
class HttpClientTest extends TestCase
{
    /** @var HttpClient */
    private $httpClient;

    const URL = 'any_url';

    public function setUp()
    {
        $this->httpClient = new HttpClient(true);
    }

    public function testGet()
    {
        $resource = $this->httpClient->get(self::URL);

        $this->assertIsResource($resource);
    }
}