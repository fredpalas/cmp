# CMP Import Video Test

Application Made with Symfony

## Installation 

### Requirements

PHP 7.1 >=

[Composer](https://getcomposer.org/download/) need for install the app

### Instruction 

```bash
composer install
bin/console doctrine:migrations:migrate
```

## Usage

```bash
bin/console app:import-video {provider}  
```

Provider list
- flub
- glorf

## Test
Made With PhpUnit and [Symfony PHPUnit Bridge](https://symfony.com/doc/current/testing.html#the-phpunit-testing-framework)

```bash
bin/phpunit
```

This command will install automatically PHPUnit 7.5 and run the test

## Location

### Source
All code are locate on src

- **Application** referes to application Class
- **Domain** All domain logic
- **Infratucture** all the elements necesary for run the application

### Test

- **tests** follow the src estructure

## Questionary

* Was it your first time writing a unit test, using a particular framework, etc?

- No, I'm using PhpUnit with Symfony PHPUnit Bridge

* What would you have done differently if you had had more time

- Improve the creating a new Video application for not using Static Call for improve the the tests






